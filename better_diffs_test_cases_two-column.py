#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import pywikibot

from pywikibot.comms import http


class Diffs(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # Only for test.wikipedia
        test_cases = [
{'title': 'Lithium_aluminate', 'diff': 552199, 'oldid': 546537},
{'title': 'Zayed_University', 'diff': 552200, 'oldid': 547110},
{'title': '13th_Venice_International_Film_Festival', 'diff': 552271, 'oldid': 485968},
{'title': 'SpaceX_Mars_transportation_infrastructure', 'diff': 552272, 'oldid': 547056},
{'title': 'Y%C3%AAn_B%C3%A1i_Province', 'diff': 552419, 'oldid': 552377},
{'title': 'Y%C3%AAn_B%C3%A1i_Province', 'diff': 552420, 'oldid': 552419},
{'title': 'Strathbungo', 'diff': 552424, 'oldid': 540376},
{'title': 'Strathbungo', 'diff': 552425, 'oldid': 552424},
{'title': 'Tto_page_9', 'diff': 552426, 'oldid': 202872},
{'title': 'Strathbungo', 'diff': 552425, 'oldid': 540376},
{'title': 'Curium', 'diff': 552443, 'oldid': 470454},
{'title': 'Earl_of_Tyrone', 'diff': 552444, 'oldid': 550515},
{'title': 'MediaWiki%3AFoobar.js', 'diff': 552527, 'oldid': 552526},
{'title': 'MediaWiki%3AFoobar.js', 'diff': 552528, 'oldid': 552526},
{'title': 'Exponential_factorial', 'diff': 551697, 'oldid': 447426},
{'title': 'Kucing', 'diff': 551694, 'oldid': 485108},
{'title': 'Zimbabwe_flyafrica.com', 'diff': 551695, 'oldid': 547133},
{'title': 'Kogia_breviceps', 'diff': 551696, 'oldid': 551688},
{'title': 'Puerto_Rico', 'diff': 552179, 'oldid': 552176},
{'title': 'Swakopmund', 'diff': 552104, 'oldid': 547079},
{'title': 'Reparatus', 'diff': 552183, 'oldid': 513078},
{'title': 'BetterDiffs1', 'diff': 552101, 'oldid': 552099},
{'title': 'Earl_of_Tyrone', 'diff': 552457, 'oldid': 552456},
{'title': 'Hazaragi_cuisine', 'diff': 552098, 'oldid': 552097},
{'title': 'Hazaragi_cuisine', 'diff': 552097, 'oldid': 447936},
{'title': 'BetterDiffs1', 'diff': 552103, 'oldid': 552102},
{'title': 'Lucre%C8%9Biu_P%C4%83tr%C4%83%C8%99canu', 'diff': 551700, 'oldid': 532796},
{'title': 'Kirchliche_Arbeit_Alpirsbach', 'diff': 552507, 'oldid': 513390},
{'title': 'Kirchliche_Arbeit_Alpirsbach', 'diff': 552510, 'oldid': 513390},
{'title': 'Israeli_Nano_Satellite_Association', 'diff': 556412, 'oldid': 513797},
{'title': 'Israeli_Nano_Satellite_Association', 'diff': 556413, 'oldid': 556412},
{'title': 'Israeli_Nano_Satellite_Association', 'diff': 556414, 'oldid': 556413},
{'title': 'Israeli_Nano_Satellite_Association', 'diff': 556415, 'oldid': 556414},
{'title': 'Yên_Bái_Province', 'diff': 552374, 'oldid': 548024},
{'title': 'Yên_Bái_Province', 'diff': 552375, 'oldid': 552374},
{'title': 'Yên_Bái_Province', 'diff': 552377, 'oldid': 552376},
{'title': 'Yên_Bái_Province', 'diff': 552376, 'oldid': 552375},
{'title': 'Earl_of_Tyrone', 'diff': 552456, 'oldid': 552444},
{'title': 'Test', 'diff': 475801, 'oldid': 476730},
{'title': 'Multipage_poem_test/a', 'diff': 552501, 'oldid': 183798},
{'title': 'Wikip%C3%A9dia:Administradores/Pedidos_de_aprova%C3%A7%C3%A3o/!SilentTest/17', 'diff': 552502, 'oldid': 326572},
{'title': 'Wikipédia:Administradores/Pedidos_de_aprovação/!SilentTest/17', 'diff': 552503, 'oldid': 552502},
{'title': 'Reparatus', 'diff': 552183, 'oldid': 513078},
{'title': 'Im_in_ur_page', 'diff': 552552, 'oldid': 75550},
{'title': 'Nicaea_of_Macedon', 'diff': 552553, 'oldid': 459838},
{'title': 'Arboretum_de_la_Pipe_Qui_Fume', 'diff': 552554, 'oldid': 485827},
{'title': 'Hakon_Sunnivasson', 'diff': 552555, 'oldid': 515170},
{'title': 'Task_Force_76', 'diff': 552556, 'oldid': 541391},
{'title': 'Test_3', 'diff': 552558, 'oldid': 473609},
{'title': 'OshwahSpeedyTest', 'diff': 552562, 'oldid': 552561},
{'title': 'Test_cookie_problem11', 'diff': 552563, 'oldid': 395728},
{'title': 'St_Martin%27s_House', 'diff': 552550, 'oldid': 511874},
{'title': 'Favilla_Guitars', 'diff': 552551, 'oldid': 540971},
{'title': 'Comparison_of_the_healthcare_systems_in_Canada_and_the_United_States', 'diff': 552773, 'oldid': 546463},
{'title': 'Herkogamy', 'diff': 552547, 'oldid': 485736},
{'title': 'Navigable_TOC', 'diff': 552910, 'oldid': 397298},
{'title': 'LegIt_test_page', 'diff': 552911, 'oldid': 384426},
{'title': 'Page383', 'diff': 552912, 'oldid': 372798},
{'title': 'Krishnalal_Basak', 'diff': 552913, 'oldid': 489245},
{'title': 'Reinier_de_Graaf_%28architect%29', 'diff': 556417, 'oldid': 552005},
{'title': 'Template:No_article_text', 'diff': 455803, 'oldid': 455804}
        ]

        with open("current_vs_new_wikidiff2_test_cases_two-column.html", "w") as output:
            output.write('<html><head><link rel="stylesheet" href="https://www.mediawiki.org/w/load.php?modules=mediawiki.diff.styles&only=styles"></head><body>')

            for test_case in test_cases:

                # Two-column wikitext diff
                two_column_diff = self.site.compare(test_case['oldid'], test_case['diff'])

                # New wikidiff2
                old_wikitext = pywikibot.Page(self.site, test_case['title']).getOldVersion(oldid=test_case['oldid'])
                new_wikitext = pywikibot.Page(self.site, test_case['title']).getOldVersion(oldid=test_case['diff'])
                new_wikidiff2_diff = http.session.post("https://wikidiff2-demo.wmcloud.org/demo.php", {'lhs': old_wikitext, 'rhs': new_wikitext, 'options[numContextLines]': 2, 'options[changeThreshold]': 0.2, 'options[movedLineThreshold]': 0.4, 'options[maxMovedLines]': 100, 'options[maxWordLevelDiffComplexity]': 40000000, 'options[maxSplitSize]': 3, 'options[initialSplitThreshold]': 0.1, 'options[finalSplitThreshold]': 0.6}).text

                output.write('<h1>Revisions: from {} to {}</h1><h2>Existing two-column diff</h2><a href="{}://{}{}/index.php?title={}&oldid={}&diff={}">View on wiki</a><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>'.format(test_case['oldid'], test_case['diff'], self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), test_case['title'], test_case['oldid'], test_case['diff']))
                output.write('<tr><td colspan=2><a href="{}://{}{}/index.php?title={}&oldid={}">{}</a></href></td><td colspan=2><a href="{}://{}{}/index.php?title={}&oldid={}">{}</a></tr>'.format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), test_case['title'], test_case['oldid'], test_case['oldid'], self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), test_case['title'], test_case['diff'], test_case['diff']))
                output.write(two_column_diff)
                output.write("</tbody></table>")

                output.write('<h2>New wikidiff2 diff</h2><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>')
                output.write(new_wikidiff2_diff)
                output.write('</tbody></table>')

            output.write("</body></html>")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Diffs(*args)
    app.run()


if __name__ == '__main__':
    main()
