#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import pywikibot

from pywikibot.comms import http


class Diffs(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-o', '--old', type=int, required=True)
        parser.add_argument('-n', '--new', type=int, required=True)
        parser.add_argument('-t', '--title', required=True)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # Two-column wikitext diff
        two_column_diff = self.site.compare(self.options.old, self.options.new)

        # New wikidiff2
        old_wikitext = pywikibot.Page(self.site, self.options.title).getOldVersion(oldid=self.options.old)
        new_wikitext = pywikibot.Page(self.site, self.options.title).getOldVersion(oldid=self.options.new)
        new_wikidiff2_diff = http.session.post("https://wikidiff2-demo.wmcloud.org/demo.php", {'lhs': old_wikitext, 'rhs': new_wikitext, 'options[numContextLines]': 2, 'options[changeThreshold]': 0.2, 'options[movedLineThreshold]': 0.4, 'options[maxMovedLines]': 100, 'options[maxWordLevelDiffComplexity]': 40000000, 'options[maxSplitSize]': 3, 'options[initialSplitThreshold]': 0.1, 'options[finalSplitThreshold]': 0.6}).text

        with open("{}_{}_two_column_vs_new_wikidiff2.html".format(self.options.old, self.options.new), "w") as output:
            output.write('<html><head><link rel="stylesheet" href="https://www.mediawiki.org/w/load.php?modules=mediawiki.diff.styles&only=styles"></head><body><h1>Revisions: from {} to {}</h1><h2>Existing two-column diff</h2><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>'.format(self.options.old, self.options.new))
            output.write('<tr><td colspan=2><a href="{}://{}{}/index.php?title={}&oldid={}">{}</a></href></td><td colspan=2><a href="{}://{}{}/index.php?title={}&oldid={}">{}</a></tr>'.format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), self.options.title, self.options.old, self.options.old, self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), self.options.title, self.options.new, self.options.new))
            output.write(two_column_diff)
            output.write("</tbody></table>")

            output.write('<h2>New wikidiff2 diff</h2><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>')
            output.write(new_wikidiff2_diff)
            output.write("</tbody></table></body></html>")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Diffs(*args)
    app.run()


if __name__ == '__main__':
    main()
