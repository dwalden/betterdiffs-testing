#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# This script finds two revisions at random and compares their diff output from
# MediaWiki with the wikidiff2 demo (https://wikidiff2-demo.wmcloud.org/demo.php)
# It will produce several HTML files to allow you to review the output of the diffs.

# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file (random_diffs.py) into the pywikibot "scripts" directory

# Generate and compare up to 10 random diffs:
# python3 pwb.py random_diffs -t 10 -lang:<lang> -family:<family>

import argparse
import pywikibot
import re
import random
import csv

from pywikibot.comms import http
from datetime import datetime
from bs4 import BeautifulSoup
from difflib import HtmlDiff
from itertools import product


class RandomDiffs(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        self.options = parser.parse_args(my_args)

    def normalise(self, two_column_diff, new_wikidiff2_diff):
        two_column_diff_normalised = BeautifulSoup(two_column_diff, 'html.parser')
        titles = two_column_diff_normalised.find_all('a', re.compile("mw-diff-movedpara-"))
        for title in titles:
            del title['title']
        linenos = two_column_diff_normalised.find_all("td", "diff-lineno")
        for lineno in linenos:
            lineno.decompose()
        new_wikidiff2_diff_normalised = BeautifulSoup(new_wikidiff2_diff, 'html.parser')
        linenos = new_wikidiff2_diff_normalised.find_all("td", "diff-lineno")
        for lineno in linenos:
            lineno.decompose()
        two_column_diff_normalised_list_of_lines = [str(x) for x in two_column_diff_normalised.find_all("td")]
        new_wikidiff2_diff_normalised_list_of_lines = [str(x) for x in new_wikidiff2_diff_normalised.find_all("td")]
        return two_column_diff_normalised_list_of_lines, new_wikidiff2_diff_normalised_list_of_lines

    def is_match(self, two_column_diff, new_wikidiff2_diff):
        two_column_diff_normalised_list_of_lines, new_wikidiff2_diff_normalised_list_of_lines = self.normalise(two_column_diff, new_wikidiff2_diff)
        return two_column_diff_normalised_list_of_lines == new_wikidiff2_diff_normalised_list_of_lines

    def get_stats(self, html):
        html_parsed = BeautifulSoup(html, 'html.parser')
        total_rows = len(html_parsed.find_all("tr"))
        moved_lines = len(html_parsed.find_all("a", class_="mw-diff-movedpara-left"))
        deleted_lines = len(html_parsed.find_all("td", class_="diff-deletedline diff-side-deleted")) - moved_lines
        added_lines = len(html_parsed.find_all("td", class_="diff-addedline diff-side-added")) - moved_lines
        empty_lines_left = len(html_parsed.find_all("td", class_="diff-empty diff-side-deleted"))
        empty_lines_right = len(html_parsed.find_all("td", class_="diff-empty diff-side-added"))
        return total_rows, deleted_lines, added_lines, moved_lines, empty_lines_left, empty_lines_right

    def run(self):
        """Run the bot."""
        tested_revisions = []
        filename = "two_column_vs_new_wikidiff2_random_{}_{}".format(self.site.family.langs[self.site.code], datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        total = {}
        for comb in product(["True", "False"], ["True", "False"], ["True", "False"], ["True", "False"]):
            total["".join(comb)] = {
                "is_same_default": comb[0],
                "is_same_changeThreshold": comb[1],
                "is_same_maxSplitSize": comb[2],
                "is_same_both": comb[3],
                "total": 0
            }

        with open("{}.html".format(filename), "w") as output:
            output.write('<html><head><link rel="stylesheet" href="https://www.mediawiki.org/w/load.php?modules=mediawiki.diff.styles&only=styles"></head><body>')

            pages = self.site.randompages(total=self.options.total)
            for page in pages:
                comparison = ""
                api_request = self.site.simple_request(action='query', prop='revisions',
                                                       titles=page.title(), rvlimit=10)
                api_response = api_request.submit()
                if len(list(api_response['query']['pages'].values())[0]['revisions']) > 1:
                    old, new = random.sample(list(api_response['query']['pages'].values())[0]['revisions'], 2)
                    old = old['revid']
                    new = new['revid']

                    # Two-column wikitext diff
                    two_column_diff = self.site.compare(old, new)

                    # New wikidiff2
                    old_wikitext = pywikibot.Page(self.site, page.title()).getOldVersion(oldid=old)
                    new_wikitext = pywikibot.Page(self.site, page.title()).getOldVersion(oldid=new)

                    # This uses the default parameters
                    new_wikidiff2_diff_default = http.session.post("https://wikidiff2-demo.wmcloud.org/demo.php", {'lhs': old_wikitext, 'rhs': new_wikitext, 'options[numContextLines]': 2, 'options[changeThreshold]': 0.2, 'options[movedLineThreshold]': 0.4, 'options[maxMovedLines]': 100, 'options[maxWordLevelDiffComplexity]': 40000000, 'options[maxSplitSize]': 3, 'options[initialSplitThreshold]': 0.1, 'options[finalSplitThreshold]': 0.6}).text
                    new_wikidiff2_diff_changeThreshold = http.session.post("https://wikidiff2-demo.wmcloud.org/demo.php", {'lhs': old_wikitext, 'rhs': new_wikitext, 'options[numContextLines]': 2, 'options[changeThreshold]': 0.3, 'options[movedLineThreshold]': 0.4, 'options[maxMovedLines]': 100, 'options[maxWordLevelDiffComplexity]': 40000000, 'options[maxSplitSize]': 3, 'options[initialSplitThreshold]': 0.1, 'options[finalSplitThreshold]': 0.6}).text
                    new_wikidiff2_diff_maxSplitSize = http.session.post("https://wikidiff2-demo.wmcloud.org/demo.php", {'lhs': old_wikitext, 'rhs': new_wikitext, 'options[numContextLines]': 2, 'options[changeThreshold]': 0.2, 'options[movedLineThreshold]': 0.4, 'options[maxMovedLines]': 100, 'options[maxWordLevelDiffComplexity]': 40000000, 'options[maxSplitSize]': 1, 'options[initialSplitThreshold]': 0.1, 'options[finalSplitThreshold]': 0.6}).text
                    new_wikidiff2_diff_both = http.session.post("https://wikidiff2-demo.wmcloud.org/demo.php", {'lhs': old_wikitext, 'rhs': new_wikitext, 'options[numContextLines]': 2, 'options[changeThreshold]': 0.3, 'options[movedLineThreshold]': 0.4, 'options[maxMovedLines]': 100, 'options[maxWordLevelDiffComplexity]': 40000000, 'options[maxSplitSize]': 1, 'options[initialSplitThreshold]': 0.1, 'options[finalSplitThreshold]': 0.6}).text

                    # Compare current diff implementation's output with new wikidiff2's diff output
                    is_same_default = self.is_match(two_column_diff, new_wikidiff2_diff_default)
                    is_same_changeThreshold = self.is_match(two_column_diff, new_wikidiff2_diff_changeThreshold)
                    is_same_maxSplitSize = self.is_match(two_column_diff, new_wikidiff2_diff_maxSplitSize)
                    is_same_both = self.is_match(two_column_diff, new_wikidiff2_diff_both)

                    total[str(is_same_default) + str(is_same_changeThreshold) + str(is_same_maxSplitSize) + str(is_same_both)]['total'] += 1

                    curr_total_rows, curr_deleted, curr_added, curr_moved, curr_empty_left, curr_empty_right = self.get_stats(two_column_diff)
                    default_total_rows, default_deleted, default_added, default_moved, default_empty_left, default_empty_right = self.get_stats(new_wikidiff2_diff_default)
                    changeThreshold_total_rows, changeThreshold_deleted, changeThreshold_added, changeThreshold_moved, changeThreshold_empty_left, changeThreshold_empty_right = self.get_stats(new_wikidiff2_diff_changeThreshold)
                    maxSplitSize_total_rows, maxSplitSize_deleted, maxSplitSize_added, maxSplitSize_moved, maxSplitSize_empty_left, maxSplitSize_empty_right = self.get_stats(new_wikidiff2_diff_maxSplitSize)
                    both_total_rows, both_deleted, both_added, both_moved, both_empty_left, both_empty_right = self.get_stats(new_wikidiff2_diff_both)
                    tested_revisions.append({'title': page.title(),
                                             'old': old,
                                             'new': new,
                                             "is_same_default": is_same_default,
                                             "is_same_changeThreshold": is_same_changeThreshold,
                                             "is_same_maxSplitSize": is_same_maxSplitSize,
                                             "is_same_both": is_same_both,
                                             'curr_total_rows': curr_total_rows,
                                             'curr_deleted': curr_deleted,
                                             'curr_added': curr_added,
                                             'curr_moved': curr_moved,
                                             'curr_empty_left': curr_empty_left,
                                             'curr_empty_right': curr_empty_right,
                                             'default_total_rows': default_total_rows,
                                             'default_deleted': default_deleted,
                                             'default_added': default_added,
                                             'default_moved': default_moved,
                                             'default_empty_left': default_empty_left,
                                             'default_empty_right': default_empty_right,
                                             'changeThreshold_total_rows': changeThreshold_total_rows,
                                             'changeThreshold_deleted': changeThreshold_deleted,
                                             'changeThreshold_added': changeThreshold_added,
                                             'changeThreshold_moved': changeThreshold_moved,
                                             'changeThreshold_empty_left': changeThreshold_empty_left,
                                             'changeThreshold_empty_right': changeThreshold_empty_right,
                                             'maxSplitSize_total_rows': maxSplitSize_total_rows,
                                             'maxSplitSize_deleted': maxSplitSize_deleted,
                                             'maxSplitSize_added': maxSplitSize_added,
                                             'maxSplitSize_moved': maxSplitSize_moved,
                                             'maxSplitSize_empty_left': maxSplitSize_empty_left,
                                             'maxSplitSize_empty_right': maxSplitSize_empty_right,
                                             'both_total_rows': both_total_rows,
                                             'both_deleted': both_deleted,
                                             'both_added': both_added,
                                             'both_moved': both_moved,
                                             'both_empty_left': both_empty_left,
                                             'both_empty_right': both_empty_right})

                    if not is_same_default or not is_same_changeThreshold or not is_same_maxSplitSize or not is_same_both:
                        comparison += '<h1>Revisions: from {} to {}</h1><h2>Existing two-column diff</h2><a href="{}://{}{}/index.php?title={}&oldid={}&diff={}">View on wiki</a><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>'.format(old, new, self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), page.title(), old, new)
                        comparison += '<tr><td colspan=2><a href="{}://{}{}/index.php?title={}&oldid={}">{}</a></href></td><td colspan=2><a href="{}://{}{}/index.php?title={}&oldid={}">{}</a></tr>'.format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), page.title(), old, old, self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), page.title(), new, new)
                        comparison += two_column_diff
                        comparison += "</tbody></table>"

                        if not is_same_default:
                            comparison += '<h2>New wikidiff2 diff default</h2><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>'
                            comparison += new_wikidiff2_diff_default
                            comparison += '</tbody></table>'

                        if not is_same_changeThreshold:
                            comparison += '<h2>New wikidiff2 diff changeThreshold = 0.3</h2><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>'
                            comparison += new_wikidiff2_diff_changeThreshold
                            comparison += '</tbody></table>'

                        if not is_same_maxSplitSize:
                            comparison += '<h2>New wikidiff2 diff maxSplitSize = 1</h2><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>'
                            comparison += new_wikidiff2_diff_maxSplitSize
                            comparison += '</tbody></table>'

                        if not is_same_both:
                            comparison += '<h2>New wikidiff2 diff both</h2><table class="diff"><colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup><tbody>'
                            comparison += new_wikidiff2_diff_both
                            comparison += '</tbody></table>'

                        comparison += '<h2>Match?</h2>'
                        comparison += "<table><tr><th>Default</th><th>changeThreshold = 0.3</th><th>maxSplitSize = 1</th><th>Both</th></tr>"
                        comparison += "<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr></table>".format(is_same_default, is_same_changeThreshold, is_same_maxSplitSize, is_same_both)

                        diff_filename = "{}_{}_diff.html".format(old, new)
                        comparison += "<a href={}>Diff</a>".format(diff_filename)
                        output.write(comparison)
                        with open(diff_filename, "w") as diff:
                            two_column_diff_normalised_list_of_lines, new_wikidiff2_diff_normalised_list_of_lines = self.normalise(two_column_diff, new_wikidiff2_diff_default)
                            diff.write(HtmlDiff().make_file(fromlines=two_column_diff_normalised_list_of_lines, tolines=new_wikidiff2_diff_normalised_list_of_lines))

            output.write("<h1>Total</h1>")
            output.write("<table><tr><th>Default</th><th>changeThreshold = 0.3</th><th>maxSplitSize = 1</th><th>Both</th><th>Total</th></tr>")
            for comb in total.values():
                output.write("<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>".format(comb["is_same_default"], comb["is_same_changeThreshold"], comb["is_same_maxSplitSize"], comb["is_same_both"], comb["total"]))

            output.write("</table></body></html>")

            with open("{}_stats.csv".format(filename), "w") as stats_output:
                fieldnames = ['title', 'old', 'new', "is_same_default", "is_same_changeThreshold", "is_same_maxSplitSize", "is_same_both", 'curr_total_rows', 'curr_deleted', 'curr_added', 'curr_moved', 'curr_empty_left', 'curr_empty_right', 'default_total_rows', 'default_deleted', 'default_added', 'default_moved', 'default_empty_left', 'default_empty_right', 'changeThreshold_total_rows', 'changeThreshold_deleted', 'changeThreshold_added', 'changeThreshold_moved', 'changeThreshold_empty_left', 'changeThreshold_empty_right', 'maxSplitSize_total_rows', 'maxSplitSize_deleted', 'maxSplitSize_added', 'maxSplitSize_moved', 'maxSplitSize_empty_left', 'maxSplitSize_empty_right', 'both_total_rows', 'both_deleted', 'both_added', 'both_moved', 'both_empty_left', 'both_empty_right']
                writer = csv.DictWriter(stats_output, fieldnames=fieldnames)
                writer.writeheader()
                writer.writerows(tested_revisions)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = RandomDiffs(*args)
    app.run()


if __name__ == '__main__':
    main()
