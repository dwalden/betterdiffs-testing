#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# This script finds two revisions at random and compares their diff output from
# MediaWiki with the wikidiff2 demo (https://wikidiff2-demo.wmcloud.org/demo.php)
# It will produce several HTML files to allow you to review the output of the diffs.

# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file (random_diffs_inline_two-column.py) into the pywikibot "scripts" directory

# Generate up to 10 random diffs:
# python3 pwb.py random_diffs_stress -t 10 -lang:<lang> -family:<family>

import argparse
import pywikibot
import random


class bcolors:
    FAIL = '\033[91m'
    ENDC = '\033[0m'


class RandomDiffs(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        pages = self.site.randompages(total=self.options.total,
                                      # namespaces="0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|100|101|119|118|190|191|482|483|710|711|5500|2600|2302|2303|2301|2300|1729|1728|1705|1704|829|828|5501")
                                      namespaces="0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|482|483|710|711|5500|2302|2303|2301|2300|1729|1728|829|828|5501")
        for page in pages:
            api_request = self.site.simple_request(action='query', prop='revisions',
                                                   titles=page.title(), rvlimit=10)
            api_response = api_request.submit()
            if len(list(api_response['query']['pages'].values())[0]['revisions']) > 1:
                old, new = random.sample(list(api_response['query']['pages'].values())[0]['revisions'], 2)
                old = old['revid']
                new = new['revid']
                pywikibot.output("Testing {} {}".format(new, old))
                two_column_diff = self.site.compare(old, new)
                inline_diff = self.site.compare(old, new, "inline")
                if type(two_column_diff) is not str or type(inline_diff) is not str:
                    pywikibot.output("{}Some issue running: {} {}{}".format(bcolors.HEADER, old, new, bcolors.ENDC))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = RandomDiffs(*args)
    app.run()


if __name__ == '__main__':
    main()
